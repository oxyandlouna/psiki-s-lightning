- Pour lancer le programme vous devez exécuter le fichier :

Trophees_NSI_V2_mainloop.py



- Il est nécessaire que vous possédiez la version 3.11 de python
ainsi que le module Pygame. Le jeu se joue avec un clavier uniquement.
Il a été programmé sous Windows


Les fichiers suivants contiennent: 

Trophee_NSI_v2_game : Contient les fonctions principales du jeu exécutés par mainloop 

Trophee_NSI_v2_import : Importe toutes les images de l'animation du personnage

Trophee_NSI_v2_inventaire : Affiche l'inventaire et ses fonctionnalités 

Trophee_NSI_v2_menu : Affiche le menu et ses fonctionnalités

Trophee_NSI_v2_settings : Intialisation des maps et des paramètres

Trophee_NSI_v2_staticobjet : Créations de blocks


Le répertoire Visuel contient toutes les images du jeu ainsi que les bruitages et la musique.




- Lorsque que le fichier Trophees_NSI_V2_mainloop.py est exécuté :


Pour faire défiler les images de l'introduction, actionner N pour voir les suivantes et B pour revoir les précédentes 


_Dans le menu :_

Vous pouvez faire défiler les élements du menu avec les flèches directionnelles 
haut et bas.

Si vous vous trouvez sur credit, option ou tutoriel (si le texte s'affiche en rouge) appuyez sur F pour interagir. Si vous souhaitez en sortir appuyez sur O

Lorsque vous êtes sur play vous pouvez lancer le jeu avec P



_Dans le jeu :_

Déplacement vers la droite : ->
Déplacement vers la gauche : <-
Saut : espace
Attaque: A   (bien qu'il n'y ait pas de monstres à attaquer dans cette version)


Vous pouvez accéder à un inventaire en appuyant sur e
/!\ Le personnage se déplace en fond /!\
Pour voir les différents items, appuyez sur les flèches -> et <- 
La touche O permet d'en sortir 

Vous pouvez retourner dans le menu grâce à la touche M

Lorsque vous arrivez à la fin d'un parcours vous arriverez devant un block
jaune, si vous entrez en contact avec vous finirez le niveau.
Lorsque l'image "victoire" apparait, vous pourrez basculer au niveau suivant avec F. 


Enfin au bout de 4 niveaux, vous aurez les images de crédit.


/!\ Le game over n’est pas encore implémenté, il faut donc relancer le jeu. /!\
Aussi pour changer de map il faut changer la variable "level_choice =  " soit par 0, 1, 2 ou 3.
